<?php

/**
 * @file
 * This file provides the functionality to Anonymize User Data.
 */

require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');

/**
 * Callback function for hook_menu().
 */
function _anonymize_users_data() {
  $users = entity_load('user');
  $chunks = array_chunk($users, 20);

  foreach ($chunks as $chunk) {
    $operations[] = array('user_anonymize_op', array($chunk));
  }
  $batch = array(
    'operations' => $operations,
    'title' => t('User Anonymize'),
    'init_message' => t('Initializing User Anonymize Operation'),
    'error_message' => t('An error occurred'),
    'finished' => 'user_anonymize_op_finished',
    'progress_message' => t('Processed @current out of @total.'),
    'file' => drupal_get_path('module', 'anonymize_data') . '/anonymize_data.user.inc',
  );
  batch_set($batch);
  batch_process('admin/people');
}

/**
 * Callback function for Batch Process.
 */
function user_anonymize_op($users, &$context) {
  $i = 0;
  foreach ($users as $user) {
    $context['message'] = t('Processing user @user', array('@user' => $user->name));
    if ($user->uid != 0 && $user->uid != 1) {
      $user->name = $i . '-' . $user->name;
      $user->pass = user_hash_password('blah');
      user_save($user);
      $i++;
    }
  }
}

/**
 * Batch operation finished callback.
 */
function user_anonymize_op_finished($success, $results, $operations) {
  if ($success) {
    $message = count($results) . ' processed.';
  }
  drupal_set_message($message);
}
