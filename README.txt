Description :
------------

This module basically used to anonymize user's and node's data. Anonymize means convert original data to some other form.

* Anonymize User Data: This option basically anonymize username and user password.

* Anonymize Node Data: This option basically anonymize node's fields data.

Working of module:
------------------
1. Go to 'admin/config/content'.
2. There you will find two options:
   * Anonymize Node Data
   * Anonymize User Data
3. Select any option according to your requirement.

Installation :
--------------

1. Download the project from https://www.drupal.org/background_video and unzip
the project.
2. Place the project under '/sites/all/modules/contrib' directory.




CONTACT :
---------

Current maintainers:
  * Pankaj Sachdeva (pankajsachdeva) - http://drupal.org/u/pankajsachdeva


This project has been sponsored by:
  * ]INIT[ AG
    Visit https://www.init.de/en for more information.

