<?php

/**
 * @file
 * This file provides the functionality for Node Data Anonymization.
 */

/**
 * Implements hook_form().
 */
function anonymize_data_node_form($form, &$form_state) {
  $form = array();

  $form['anonymize_data_content_types'] = array(
    '#title' => t('Select Content Types'),
    '#type' => 'checkboxes',
    '#description' => t('Please select the content types for which you want to anonymize the data'),
    '#options' => _get_content_types(),
    '#required' => TRUE,
  );

  $form['#submit'][] = 'anonymize_data_node_form_submit';

  return system_settings_form($form);
}

/**
 * Callback function to get all content types.
 */
function _get_content_types() {
  $content_types = node_type_get_types();
  $list = array();
  foreach ($content_types as $key => $content_type) {
    $list[$key] = $content_type->name;
  }
  return $list;
}

/**
 * Submit handler for Anonymize Node configuration form.
 */
function anonymize_data_node_form_submit($form, &$form_state) {
  foreach ($form_state['values']['anonymize_data_content_types'] as $value) {
    if ($value) {
      $nodes = node_load_multiple(array(), array('type' => $value));
      $fields = field_info_instances('node', $value);
      $chunks = array_chunk($nodes, 20);
      foreach ($chunks as $chunk) {
        $operations[] = array('anonymize_data_batch_op', array($nodes, $fields));
      }
    }
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'anonymize_data_batch_op_finished',
    'title' => t('Anonymize Node Data'),
    'init_message' => t('Initializing Node Anonymize Operation'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Fix has encountered an error.'),
    'file' => drupal_get_path('module', 'anonymize_data') . '/anonymize_data.node.inc',
  );

  batch_set($batch);
}

/**
 * Callback function for Anonymize Node's fields value(s)
 */
function anonymize_data_batch_op($nodes, $fields, &$context) {
  $dummy_image = _get_dummy_image();
  $dummy_file = _get_dummy_file();
  foreach ($nodes as $node) {
    $context['message'] = t('Processing node of @node', array('@node' => $node->type));
    foreach ($fields as $field_name => $field) {
      // Anonymize Textfields data.
      $field_info = field_info_field($field_name);
      $field_type = $field_info['type'];

      if (($field_type == 'text_with_summary' || $field_type == 'text_long' || $field_type == 'text') && isset($node->{$field_name}[LANGUAGE_NONE])) {
        // For single value textfield/Long text and Summary.
        if (count($node->{$field_name}[LANGUAGE_NONE]) == 1) {
          $node->{$field_name}[LANGUAGE_NONE][0]['value'] = strrev($node->{$field_name}[LANGUAGE_NONE][0]['value']);
        }
        // For multi-value textfield/Long text and Summary.
        else {
          foreach ($node->{$field_name}[LANGUAGE_NONE] as $key => $value) {
            $node->{$field_name}[LANGUAGE_NONE][$key]['value'] = strrev($node->{$field_name}[LANGUAGE_NONE][$key]['value']);
          }
        }
      }

      // For Single and Multi-value Image Anonymize Data.
      if ($field_type == 'image' && isset($node->{$field_name}[LANGUAGE_NONE])) {
        if (count($node->{$field_name}[LANGUAGE_NONE]) == 1) {
          $node->{$field_name}[LANGUAGE_NONE][0]['fid'] = $dummy_image->fid;
        }
        else {
          foreach ($node->{$field_name}[LANGUAGE_NONE] as $key => $value) {
            $node->{$field_name}[LANGUAGE_NONE][$key]['fid'] = $dummy_image->fid;
          }
        }
      }

      // For Single and Multi-value File Field Data Anonymize.
      if ($field_type == 'file' && isset($node->{$field_name}[LANGUAGE_NONE])) {
        if (count($node->{$field_name}[LANGUAGE_NONE]) == 1) {
          $node->{$field_name}[LANGUAGE_NONE][0]['fid'] = $dummy_image->fid;
        }
        else {
          foreach ($node->{$field_name}[LANGUAGE_NONE] as $key => $value) {
            $node->{$field_name}[LANGUAGE_NONE][$key]['fid'] = $dummy_file->fid;
          }
        }
      }

      // For Single and multi-valued Lists.
      if (($field_type == 'list_float' || $field_type == 'list_integer' || $field_type == 'list_text') && isset($node->{$field_name}[LANGUAGE_NONE])) {
        $list_values = list_allowed_values($field_info);
        if (count($node->{$field_name}[LANGUAGE_NONE]) == 1) {
          $node->{$field_name}[LANGUAGE_NONE][0]['value'] = $list_values[array_rand($list_values)];
        }
        // For multi-value textfield/Long text and Summary.
        else {
          foreach ($node->{$field_name}[LANGUAGE_NONE] as $key => $value) {
            $node->{$field_name}[LANGUAGE_NONE][$key]['value'] = $list_values[array_rand($list_values)];
          }
        }
      }

      if ($field['widget']['type'] == 'number') {
        // For single valued Number fields (Integer, float, decimal).
        if (count($node->{$field_name}[LANGUAGE_NONE]) == 1) {
          $node->{$field_name}[LANGUAGE_NONE][0]['value'] = rand(0, 1000);
        }
        // For multi-valued Number fields (Integer, float, decimal).
        else {
          foreach ($node->{$field_name}[LANGUAGE_NONE] as $key => $value) {
            $node->{$field_name}[LANGUAGE_NONE][$key]['value'] = rand(0, 1000);
          }
        }
      }
    }
    field_attach_update('node', $node);
  }
}

/**
 * Callback function for Batch Process.
 */
function anonymize_data_batch_op_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Nodes have been anonymized.'));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ));
    drupal_set_message($message, 'error');
  }
}

/**
 * Helper function to get dummy image from module.
 */
function _get_dummy_image() {
  $file_path = drupal_get_path('module', 'anonymize_data') . '/drupal_logo-spot_blue.png';
  $file_content = file_get_contents($file_path);
  $file = file_save_data($file_content);
  return $file;
}

/**
 * Helper function to get dummy file.
 */
function _get_dummy_file() {
  global $base_url;
  $file_path = drupal_get_path('module', 'anonymize_data') . '/dummy_pdf.pdf';
  $file_content = file_get_contents($base_url . '/' . $file_path);
  $file = file_save_data($file_content);
  return $file;
}
